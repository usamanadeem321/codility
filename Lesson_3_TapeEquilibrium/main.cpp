#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <numeric>
#include <vector>
using namespace std;

int solution(vector<int> &main_vector) {
  if (main_vector.size() == 1)
    return main_vector[0];
  else if (main_vector.size() == 2)
    return abs(main_vector[0] - main_vector[1]);

  vector<int> abs_diff_vec;
  long int sum_upto_p, sum_from_p_to_end, total_sum, abs_diff;
  long int min = numeric_limits<int>::max();

  // sum of main vector
  total_sum = accumulate(main_vector.begin(), main_vector.end(), 0);
  sum_upto_p = 0;
  for (auto i = 0; i < main_vector.size() - 1; ++i) {
    sum_upto_p += main_vector[i];
    sum_from_p_to_end = total_sum - sum_upto_p;
    abs_diff = abs(sum_from_p_to_end - sum_upto_p);
    cout << "sum_upto_p: " << sum_upto_p << endl;
    cout << "sum_from_p_to_end: " << sum_from_p_to_end << endl;
    cout << "abs_diff: " << abs_diff << endl;
    cout << "i: " << i << endl;
    cout << endl;
    if (abs_diff < min)
      min = abs_diff;
  }
  return min;
}

int main() {

  vector<int> vec_1;
  vec_1.push_back(-10);
  vec_1.push_back(-20);
  vec_1.push_back(-30);
  vec_1.push_back(-40);
  vec_1.push_back(100);

  int result = solution(vec_1);
  cout << "Result:    " << result << endl;
}