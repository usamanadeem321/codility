#include <algorithm>
#include <iostream>
#include <map>
#include <vector>

using namespace std;

int solution(int K, vector<int> &A) {
  map<int, int> dict;
  for (int i = 0; i < A.size(); i++) {
    dict[A[i]] += 1;
    if (dict.size() == K) {
      cout << "Map Size:    " << dict.size() << endl;
      return i;
    }
  }
  return -1;
}

int main() {
  vector<int> A;
  A.push_back(1);
  A.push_back(3);
  A.push_back(1);
  A.push_back(4);
  A.push_back(2);
  A.push_back(3);
  A.push_back(5);
  A.push_back(4);

  int a = solution(5, A);
  cout << "Value Returned:  " << a << endl;
}