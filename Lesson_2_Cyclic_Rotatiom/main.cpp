#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

void disp(vector<int> v) {
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
}

vector<int> solution(vector<int> &A, int K) {
  vector<int> temp;
  if (A.size() == 1 || A.size() == 0 || K == A.size())
    return A;
  else if (K > A.size()) {
    while (K > A.size())
      K = K - A.size();
  }

  for (int i = 0; i < A.size(); i++) {
    if (i < K)
      temp.push_back(A[A.size() - K + i]);
    else
      temp.push_back(A[i - K]);
  }
  return temp;
}

int main() {
  vector<int> arr;

  disp(arr);
  cout << endl;
  disp(solution(arr, 2));
  return 0;
}