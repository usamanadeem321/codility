#include <iostream>
#include <map>
#include <string>
#include <vector>
using namespace std;

void disp(const vector<int> &v) {
  cout << endl;
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  cout << endl;
}

vector<int> solution(string &S, vector<int> &P, vector<int> &Q) {

  vector<vector<int>> arr;
  vector<int> res;

  for (auto i = 0; i < S.length(); ++i) {
    cout << "i:  " << i << endl;
    char c = S[i];
    if (c == 'A')
      arr.push_back({1, 0, 0, 0});

    else if (c == 'C')
      arr.push_back({0, 1, 0, 0});
    else if (c == 'G')
      arr.push_back({0, 0, 1, 0});
    else if (c == 'T')
      arr.push_back({0, 0, 0, 1});
  }

  cout << "Computing Prefixes:" << endl;

  for (auto i = 1; i < S.length(); ++i) {
    for (auto j = 0; j < 4; ++j) {
      arr[i][j] += arr[i - 1][j];
    }
  }

  for (int i = 0; i < P.size(); i++) {

    for (int a = 0; a < 4; a++) {

      int sub = 0;
      if (P[i] - 1 >= 0)
        sub = arr[P[i] - 1][a];
      if (arr[Q[i]][a] - sub > 0) {
        cout << a + 1 << endl;
        res.push_back(a + 1);
        break;
      }
    }
  }
  return res;
}

int main() {
  // int P[3];
  // P[0] = 2;
  // P[0] = 5;
  // P[0] = 0;

  // int Q[3];
  // Q[0] = 2;
  // Q[0] = 5;
  // Q[0] = 0;

  vector<int> P;
  vector<int> Q;
  P.push_back(2);
  P.push_back(5);
  P.push_back(0);

  Q.push_back(4);
  Q.push_back(5);
  Q.push_back(6);
  string st = "CAGCCTA";
  vector<int> result = solution(st, P, Q);
  disp(result);
}