#include <iostream>
#include <map>
#include <vector>
using namespace std;

void disp(const vector<int> &v) {
  cout << endl;
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  cout << endl;
}

vector<int> solution(string &DNA_seq, vector<int> &query_1,
                     vector<int> &query_2) {
  vector<int> min;

  int value = 5;

  for (unsigned int i = 0; i < query_1.size(); ++i) {
    cout << endl;
    cout << "i: " << i << endl;
    disp(min);
    value = 5;
    for (unsigned int j = query_1[i]; j <= query_2[i]; ++j) {
      cout << "j: " << j << endl;
      if (DNA_seq[j] == 'A') {
        value = 1;
        break;
      }

      else if (DNA_seq[j] == 'C' && value > 2) {
        value = 2;
      }

      else if (DNA_seq[j] == 'G' && value > 3) {
        value = 3;

      } else if (DNA_seq[j] == 'T' && value > 4) { // write full condition
        value = 4;
      }
    }
    cout << "Value: " << value << endl;
    min.push_back(value);
  }

  return min;
}

int main() {
  vector<int> P;
  vector<int> Q;
  P.push_back(2);
  P.push_back(5);
  P.push_back(0);

  Q.push_back(4);
  Q.push_back(5);
  Q.push_back(6);
  string st = "CAGCCTA";
  vector<int> result = solution(st, P, Q);

  cout << "Result:   " << endl;
  disp(result);
}