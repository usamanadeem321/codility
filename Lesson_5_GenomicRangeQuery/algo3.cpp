#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>
using namespace std;

void disp(const vector<int> &v) {
  cout << endl;
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  cout << endl;
}

vector<int> solution(string &DNA_seq, vector<int> &query_1,
                     vector<int> &query_2) {
  vector<int> min;

  for (unsigned int i = 0; i < query_1.size(); ++i) {
    string str2 = DNA_seq.substr(query_1[i], abs(query_2[i] - query_1[i]) + 1);

    if (str2.find('A') != string::npos)
      min.push_back(1);

    else if (str2.find('C') != string::npos)

      min.push_back(2);

    else if (str2.find('G') != string::npos)

      min.push_back(3);

    else
      min.push_back(4);
  }

  return min;
}

int main() {
  vector<int> P;
  vector<int> Q;
  P.push_back(2);
  P.push_back(5);
  P.push_back(0);

  Q.push_back(4);
  Q.push_back(5);
  Q.push_back(6);
  string st = "CAGCCTA";
  vector<int> result = solution(st, P, Q);

  cout << "Result:   " << endl;
  disp(result);
}