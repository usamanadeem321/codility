#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
using namespace std;

void disp(const vector<int> &v) {
  cout << endl;
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  cout << endl;
}

vector<int> solution(string &DNA_seq, vector<int> &query_1,
                     vector<int> &query_2) {
  vector<int> min(query_1.size(), 4);
  vector<int> DNA_seq_conv(0, 4);
  map<char, int> DNA_map;
  DNA_map.insert(pair<char, int>('A', 1));
  DNA_map.insert(pair<char, int>('C', 2));
  DNA_map.insert(pair<char, int>('G', 3));
  DNA_map.insert(pair<char, int>('T', 4));

  unsigned int j = 0;

  for (j; j < DNA_seq.length(); ++j) {
    DNA_seq_conv.push_back(DNA_map[DNA_seq[j]]);
  }
  disp(DNA_seq_conv);

  for (unsigned int i = 0; i < query_1.size(); ++i) {

    min[i] = *min_element(DNA_seq_conv.begin() + query_1[i],
                          DNA_seq_conv.begin() + query_2[i] + 1);
    cout << "Min:   " << min[i] << endl;
    cout << endl;
  }

  return min;
}

int main() {
  vector<int> P;
  vector<int> Q;
  P.push_back(2);
  P.push_back(5);
  P.push_back(0);

  Q.push_back(4);
  Q.push_back(5);
  Q.push_back(6);
  string st = "CAGCCTA ";
  vector<int> result = solution(st, P, Q);

  cout << "Result:   " << endl;
  disp(result);
}