#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int solution(vector<int> &A) {
  if (A.size() == 1) {
    if (A[0] == 1) {
      return 1;
    } else {
      return 0;
    }
  }

  else if (A.size() == 0) {
    return 0;
  }

  // 2 3 1 5
  int missing_elem = -1;
  sort(A.begin(), A.end());

  if (A[0] != 1) {
    return 0;
  }
  // 1 3 4
  for (int i = 1; i < A.size(); i++) {
    if (A[i - 1] + 1 != A[i]) {
      missing_elem = A[i - 1] + 1;
      break;
    }
  }

  if (missing_elem == -1) {
    return 1;
  } else {
    return 0;
  }
}
int main() {
  vector<int> A;
  // A.push_back(2);
  A.push_back(3);
  A.push_back(1);
  A.push_back(4);

  cout << solution(A) << endl;
}
