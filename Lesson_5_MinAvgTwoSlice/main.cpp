#include <iostream>
#include <limits>
#include <vector>

using namespace std;

int solution(vector<int> &main_vec)
{
   if (main_vec.size() <= 2)
   {
      return 0;
   }

   float avg_2;
   float avg_3;
   float min_avg = numeric_limits<int>::max();
   int   min_avg_idx;

   for (auto idx = 2; idx < main_vec.size(); ++idx)
   {
      avg_2 = (main_vec[idx - 1] + main_vec[idx - 2]) / (float)2;
      avg_3 = (main_vec[idx] + main_vec[idx - 1] + main_vec[idx - 2]) / (float)3;
      if (avg_2 < min_avg)
      {
         min_avg     = avg_2;
         min_avg_idx = idx - 2;
         cout << "Average of 2 is:  " << min_avg << endl;
         cout << "starting index:    " << min_avg_idx << endl;
      }
      if (avg_3 < min_avg)
      {
         min_avg     = avg_3;
         min_avg_idx = idx - 2;
         cout << "Average of 3 is:  " << min_avg << endl;
         cout << "starting index:    " << min_avg_idx << endl;
      }
   }

   avg_2 = (main_vec[main_vec.size() - 1] + main_vec[main_vec.size() - 2]) / (float)2;
   if (avg_2 < min_avg)
   {
      min_avg     = avg_2;
      min_avg_idx = main_vec.size() - 2;
   }
   cout << "Min Average is:  " << min_avg << endl;
   cout << "Min Average Index is:  " << min_avg_idx << endl;
   return min_avg_idx;
}
int main()
{
   vector<int> main_vector;
   main_vector.push_back(4);
   main_vector.push_back(2);
   main_vector.push_back(2);
   main_vector.push_back(5);
   main_vector.push_back(2);
   main_vector.push_back(1);
   main_vector.push_back(1);

   int result = solution(main_vector);
   cout << "Result:  " << result << endl;
}