#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

void disp(vector<int> v) {
  cout << endl;
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  cout << endl;
}

int solution(vector<int> &main_vec) {
  int min_pos_number = 1;
  sort(main_vec.begin(), main_vec.end());
  disp(main_vec);
  for (unsigned int idx = 0; idx < main_vec.size(); ++idx) {
    cout << "Min Pos: " << min_pos_number << endl;
    cout << "Entry: " << main_vec[idx] << endl;
    cout << endl;

    if (main_vec[idx] == min_pos_number) {
      min_pos_number++;
    } else {
      if (main_vec[idx] != main_vec[idx])
        return min_pos_number;
    }
  }
  return min_pos_number;
}

int main() {
  vector<int> test_vec;
  test_vec.push_back(1);
  test_vec.push_back(1);
  test_vec.push_back(3);
  test_vec.push_back(2);
  test_vec.push_back(6);
  test_vec.push_back(4);
  int ans = solution(test_vec);
  cout << "Answer:  " << ans << endl;
}