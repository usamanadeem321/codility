#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int solution(vector<int> &v1) {
  int val;
  int freq;
  for (int i = 0; i < v1.size(); i++) {
    freq = std::count(v1.begin(), v1.end(), v1[i]);

    if (freq % 2 != 0)
      return v1[i];
  }

  return 0;
}

int main() {
  vector<int> v1;
  int size, entry;
  std::cout << "Size: ";
  cin >> size;
  std::cout << "---Entries---" << std::endl;
  for (int i = 0; i < size; i++) {
    cin >> entry;
    v1.push_back(entry);
  }
  cout << "Result:    " << solution(v1);

  return 0;
}