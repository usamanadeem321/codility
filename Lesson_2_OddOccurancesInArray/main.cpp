#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

void disp(vector<int> v) {
  cout << endl;
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
}
// 3 3 3 7 7 7 7
int solution(vector<int> &v1) {

  sort(v1.begin(), v1.end());
  // disp(v1);
  int i = 0;
  int next = 1;

  while (i < v1.size()) {
    // std::cout << i << " " << next << std::endl;
    if (v1[i] == v1[next]) {
      i = next + 1;
      next = i + 1;

    } else
      return v1[i];
  }
  return 0;
}

int main() {
  vector<int> v1;
  int size, entry;
  std::cout << "Size: ";
  cin >> size;
  std::cout << "---Entries---" << std::endl;
  for (int i = 0; i < size; i++) {
    cin >> entry;
    v1.push_back(entry);
  }
  int x = solution(v1);
  std::cout << std::endl;
  cout << "Result: " << x;

  return 0;
}