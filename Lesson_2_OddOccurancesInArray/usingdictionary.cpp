#include <iostream>
#include <map>
#include <vector>
using namespace std;

void disp(map<int, int> mp) {
  for (auto iter = mp.begin(); iter != mp.end(); iter++)
    std::cout << (*iter).first << " " << (*iter).second << std::endl;
}

int solution(vector<int> &v1) {
  map<int, int> dict;

  for (int i = 0; i < v1.size(); i++)
    dict[v1[i]] += 1;

  for (auto iter = dict.begin(); iter != dict.end(); iter++) {
    if ((*iter).second % 2 != 0)
      return (*iter).first;
  }

  return 0;
}

int main() {
  vector<int> v1;
  int size, entry;
  std::cout << "Size: ";
  cin >> size;
  std::cout << "---Entries---" << std::endl;
  for (int i = 0; i < size; i++) {
    cin >> entry;
    v1.push_back(entry);
  }
  cout << "Result:    " << solution(v1);

  return 0;
}