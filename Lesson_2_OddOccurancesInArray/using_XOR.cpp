
#include <iostream>
#include <vector>
using namespace std;

int solution(vector<int> &v1) {
  int unpaired = v1[0];

  for (int i = 1; i < v1.size(); i++) {
    cout << "Unpaired " << unpaired << endl;
    cout << "v1:    " << v1[i] << endl;
    unpaired = unpaired ^ v1[i];
    cout << "New Unpaired " << unpaired << endl;
  }

  return unpaired;
}

int main() {

  vector<int> v1;
  int size, entry;
  std::cout << "Size: ";
  cin >> size;
  std::cout << "---Entries---" << std::endl;
  for (int i = 0; i < size; i++) {
    cin >> entry;
    v1.push_back(entry);
  }

  cout << "Unpaired Element:    " << solution(v1) << endl;
  return 0;
}