#include <iostream>

int solution(int X, int Y, int D) {
  int jumps;

  if ((Y - X) % D == 0) {
    jumps = (Y - X) / D;
  } else {
    jumps = (Y - X) / D + 1;
  }
  return jumps;
}

int main() {
  int jumps = solution(10, 85, 30);
  std::cout << "Jumps: " << jumps << std::endl;
}