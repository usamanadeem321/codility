#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int solution(vector<int> &A) {
  if (A.size() == 1) {
    if (A[0] == 2) {
      return 1;
    } else {
      return A[0] + 1;
    }
  }

  else if (A.size() == 0) {
    return 1;
  }

  // 2 3 1 5
  int missing_elem;
  sort(A.begin(), A.end());
  if (A[0] != 1) {
    return 1;
  }
  // 1 2 3 5
  bool found = false;
  for (int i = 1; i < A.size(); i++) {
    if (A[i - 1] + 1 != A[i]) {
      missing_elem = A[i - 1] + 1;
      found = true;
      break;
    }
  }
  // last element missing?
  if (!found) {
    missing_elem = A[A.size() - 1] + 1;
  }

  return missing_elem;
}
int main() {
  vector<int> A;
  A.push_back(2);
  A.push_back(3);
  A.push_back(1);
  A.push_back(4);

  cout << "Missing Element:   " << solution(A);
}
