#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

void disp(vector<int> v) {
  cout << endl;
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  cout << endl;
}

vector<int> solution(int N, vector<int> &main_vec) {
  vector<int> counters;
  for (int i = 0; i < N; ++i)
    counters.push_back(0);

  int max = 0;

  for (int i = 0; i < main_vec.size(); i++) {
    disp(counters);
    if (main_vec[i] <= N) {
      counters[main_vec[i] - 1] += 1;
      if (counters[main_vec[i] - 1] > max)
        max = counters[main_vec[i] - 1];
    } else if (main_vec[i] > N) {

      // make all counter values = max
      for (int j = 0; j < counters.size(); ++j)
        counters[j] = max;
    }
  }
  return counters;
}

int main() {
  vector<int> main_vec;
  main_vec.push_back(3);
  main_vec.push_back(4);
  main_vec.push_back(4);
  main_vec.push_back(6);
  main_vec.push_back(1);
  main_vec.push_back(4);
  main_vec.push_back(4);

  vector<int> result_vec = solution(5, main_vec);
  cout << "result:  " << endl;
  disp(result_vec);
}