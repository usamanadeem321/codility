#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

void disp(const vector<int> &v) {
  cout << endl;
  for (unsigned int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  cout << endl;
}

vector<int> solution(int length_of_counters_vec,
                     const vector<int> &main_vector) {
  //   vector<int> counters;
  //   for (int i = 0; i < length_of_counters_vec; ++i)
  //     counters.push_back(0);

  vector<int> counters(length_of_counters_vec, 0);

  int temp = 0, max = 0;

  for (unsigned int i = 0; i < main_vector.size(); i++) {
    cout << "i:   " << i << endl;
    if (main_vector[i] <= length_of_counters_vec) {

      // prev value is greater than temp, meaning temp has already been added to
      // this  value at some prev iteration
      if (counters[main_vector[i] - 1] > temp)
        counters[main_vector[i] - 1] += 1;

      // temp is greater than prev value, meaning prec value should add this new
      // max value (temp) before incrementing by 1
      else if (temp > counters[main_vector[i] - 1])
        counters[main_vector[i] - 1] = temp + 1;
      else if (temp == counters[main_vector[i] - 1])
        counters[main_vector[i] - 1] += 1;

      if (max < counters[main_vector[i] - 1])
        max = counters[main_vector[i] - 1];

    }

    else {
      temp = max;
      cout << "temp: " << temp << endl;
    }
    disp(counters);
  }

  // setting those elemets to 1
  for (int i = 0; i < length_of_counters_vec; i++) {
    if (counters[i] < temp) {
      counters[i] = temp;
    }
  }

  return counters;
}

int main() {
  vector<int> main_vec;
  main_vec.reserve(10);
  main_vec.push_back(3);
  main_vec.push_back(4);
  main_vec.push_back(4);
  main_vec.push_back(6);
  main_vec.push_back(1);
  main_vec.push_back(4);
  main_vec.push_back(4);

  vector<int> result_vec = solution(5, main_vec);
  cout << "result:  " << endl;
  disp(result_vec);
}