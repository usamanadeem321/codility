#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int no_of_gaps = 0;
int gap_zeros = 0;
vector<int> binary_gap_info;

int CalculateBinaryGap() {
  binary_gap_info.push_back(gap_zeros);
  gap_zeros = 0;
  return 0;
}

void disp(vector<int> v) {
  for (int i = 1; i <= v.size(); i++) {
    std::cout << std::endl;
    std::cout << i << " Gap Zeros:    " << v[v.size() - i] << std::endl;
  }
}

int cases(int current, int prev) {

  if (current == 0) {
    gap_zeros += 1;
  } else if (current == 1 && prev == 0) {
    CalculateBinaryGap();
  }

  return 0;
}

int solution(int n) {

  int i, prev;
  int current = -1;
  while (n % 2 == 0)
    n = n / 2;
  for (i = 0; n > 0; i++) {
    prev = current;
    current = n % 2;
    n = n / 2;

    if (prev != -1) {
      cases(current, prev);
    }
  }
  // std::cout << "No of Gaps:    " << binary_gap_info.size();
  // disp(binary_gap_info);
  if (binary_gap_info.size() > 0) {
    auto it_max = max_element(binary_gap_info.begin(), binary_gap_info.end());
    return *it_max;
  } else
    return 0;
}

int main() {

  int n;
  std::cout << "Enter a number:  " << std::endl;
  cin >> n;
  int max = solution(n);
  std::cout << "Largest Gap: " << max << std::endl;
}
